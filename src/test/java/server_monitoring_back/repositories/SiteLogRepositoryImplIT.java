package server_monitoring_back.repositories;

import com.mongodb.client.MongoCollection;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import server_monitoring_back.models.Server;
import server_monitoring_back.models.SiteLog;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.db_service.DBServiceImpl;

import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;

class SiteLogRepositoryImplIT {

    final DBService dbService = new DBServiceImpl();
    SiteLogRepository siteLogRepository = new SiteLogRepositoryImpl(dbService, "site_log", new SiteLog());
    MongoCollection collection;
    SiteLog siteLog;

    @BeforeEach
    void setUp() {
        collection = dbService.getCollection("site_log", new SiteLog());
        siteLog =  new SiteLog();
        siteLog.setUserId(new ObjectId());
        siteLog.setUrl("url");
        collection.insertOne(siteLog);
    }

    @AfterEach
    void tearDown() {
        collection.deleteMany(eq("user_id",siteLog.getUserId()));
    }

    @Test
    void findLogsByUrlAndUserId() {
        List<SiteLog> siteLogs = siteLogRepository.findLogsByUrlAndUserId("url",siteLog.getUserId());
        assertEquals(siteLogs.size(),1);
        assertEquals(siteLog,siteLogs.get(0));
    }

    @Test
    void deleteByUrlAndUserId() {
        siteLogRepository.deleteByUrlAndUserId(siteLog.getUrl(),siteLog.getUserId());
        assertNull(collection.find(eq("user_id", siteLog.getUserId())).first());
    }
    @Test
    void deleteByUrlAndUserIdFail() {
        siteLogRepository.deleteByUrlAndUserId("aaaaa",new ObjectId());
        assertNotNull(collection.find(eq("user_id", siteLog.getUserId())).first());
    }
}