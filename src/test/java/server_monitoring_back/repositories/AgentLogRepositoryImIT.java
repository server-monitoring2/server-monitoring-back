package server_monitoring_back.repositories;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import server_monitoring_back.models.AgentLog;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.db_service.DBServiceImpl;

import static org.junit.jupiter.api.Assertions.*;

class AgentLogRepositoryImIT {
    public static final String PUBLIC_KEY = "aa";
    MongoCollection mongoCollection;
    final DBService dbService = new DBServiceImpl();
    AgentLogRepository agentLog = new AgentLogRepositoryImp(dbService, "agent_log", new AgentLog());
    AgentLog testAgent;

    @BeforeEach
    void setUp() {
        mongoCollection = dbService.getCollection("agent_log",new AgentLog());
        testAgent =  new AgentLog();
        testAgent.setPublicKey(PUBLIC_KEY);
        mongoCollection.insertOne(testAgent);
    }

    @AfterEach
    void tearDown() {
        mongoCollection.deleteMany(Filters.eq("public_key", PUBLIC_KEY));
    }

    @Test
    void findLogsByPublicKey() {
        assertEquals(agentLog.findLogsByPublicKey(PUBLIC_KEY).size(),1);
        assertEquals(agentLog.findLogsByPublicKey(PUBLIC_KEY).get(0), testAgent);
    }

    @Test
    void deleteByPublicKey() {
        agentLog.deleteByPublicKey(PUBLIC_KEY);
        assertEquals(agentLog.findLogsByPublicKey(PUBLIC_KEY).size(),0);
    }
}