package server_monitoring_back.repositories;

import com.mongodb.client.MongoCollection;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import server_monitoring_back.models.AgentLog;
import server_monitoring_back.models.Server;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.db_service.DBServiceImpl;

import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;

class ServerRepositoryImplIT {

    MongoCollection collection;
    final DBService dbService = new DBServiceImpl();
    ServerRepository serverRepository = new ServerRepositoryImpl(dbService, "server_info", new Server());
    Server testServer;

    @BeforeEach
    void setUp() {
        collection = dbService.getCollection("server_info",new Server());
        testServer =  new Server();
        testServer.setUserId(new ObjectId());
        testServer.setPublicKey("public");
        collection.insertOne(testServer);
    }

    @AfterEach
    void tearDown() {
        collection.deleteMany(eq("user_id",testServer.getUserId()));
    }

    @Test
    void existByPublicKey() {
        assertTrue(serverRepository.existByPublicKey("public"));
    }
    @Test
    void existByPublicKeyFalse() {
        assertFalse(serverRepository.existByPublicKey("publicasdfsadfasf"));
    }

    @Test
    void getServersByUserId() {
        List<Server> serverList = serverRepository.getServersByUserId(testServer.getUserId());
        assertEquals(testServer,serverList.get(0));
        assertEquals(serverList.size(),1);
    }

    @Test
    void updateServer() {
        Server updateServer = new Server();
        updateServer.setNewPublicKey("changed");
        updateServer.setNewPrivateKey("private");
        updateServer.setOldPublicKey(testServer.getPublicKey());
        updateServer.setUserId(testServer.getUserId());
        serverRepository.updateServer(updateServer);
        collection.find(eq("public_key","changed"));
        Server server = (Server)collection.find(eq("public_key","changed")).first();
        assert server != null;
        assertEquals(server.getPublicKey(),"changed");
        assertEquals(server.getPrivateKey(),"private");
    }

    @Test
    void getServerByPublicKey() {
        Optional<Server> serverOptional = serverRepository.getServerByPublicKey("public");
        assertEquals(testServer,serverOptional.get());
    }

    @Test
    void deleteByUserId() {
        serverRepository.deleteByUserId(testServer.getUserId());
        assertNull(collection.find(eq("user_id", testServer.getUserId())).first());
    }
    @Test
    void deleteByUserIdFalse() {
        serverRepository.deleteByUserId(new ObjectId());
        assertNotNull(collection.find(eq("user_id", testServer.getUserId())).first());
    }
}