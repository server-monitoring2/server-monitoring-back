package server_monitoring_back.repositories;

import com.mongodb.client.MongoCollection;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import server_monitoring_back.models.Site;
import server_monitoring_back.models.Tokens;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.db_service.DBServiceImpl;
import server_monitoring_back.services.token_service.TokenService;
import server_monitoring_back.services.token_service.TokenServiceImpl;

import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;

class TokensRepositoryImplIT {

    private final TokenService tokenService = new TokenServiceImpl();
    final DBService dbService = new DBServiceImpl();
    TokenRepository tokenRepository = new TokensRepositoryImpl(dbService, "user_tokens", new Tokens());
    MongoCollection collection;
    Tokens tokens;

    @BeforeEach
    void setUp() {
        collection = dbService.getCollection("user_tokens", new Tokens());
        tokens = new Tokens();
        tokens.setUserId(new ObjectId());
        tokens.setUserToken("user");
        tokens.setActivationToken("activate");
        tokens.setPasswordResetToken("password");
        collection.insertOne(tokens);
    }

    @AfterEach
    void tearDown() {
        collection.findOneAndDelete(eq("user_id",tokens.getUserId()));
    }

    @Test
    void deleteByUserId() {
        tokenRepository.deleteByUserId(tokens.getUserId());
        assertNull(collection.find(eq("user_id", tokens.getUserId())).first());
    }
    @Test
    void deleteByUserIdFail() {
        tokenRepository.deleteByUserId(new ObjectId());
        assertNotNull(collection.find(eq("user_id", tokens.getUserId())).first());
    }

    @Test
    void findByUserId() {
        Optional<Tokens> tokensOptional = tokenRepository.findByUserId(tokens.getUserId());
        assertNotNull(tokensOptional.get());
        assertEquals(tokensOptional.get(),tokens);
    }

    @Test
    void setNewResetToken() {
        Tokens updToken = tokens;
        tokenRepository.setNewResetToken(updToken);
        Tokens result = (Tokens)collection.find(eq("user_id",tokens.getUserId())).first();
        assertNotNull(result);
        assertNotEquals(result.getPasswordResetToken(),tokens.getPasswordResetToken());
    }

    @Test
    void setNewActivationToken() {
        Tokens updToken = tokens;
        tokenRepository.setNewActivationToken(updToken);
        Tokens result = (Tokens)collection.find(eq("user_id",tokens.getUserId())).first();
        assertNotNull(result);
        assertNotEquals(result.getActivationToken(),tokens.getActivationToken());
    }

    @Test
    void findByResetToken() {
        Optional<Tokens> tokensOptional = tokenRepository.findByResetToken(tokens.getPasswordResetToken());
        assertNotNull(tokensOptional.get());
        assertEquals(tokens,tokensOptional.get());
    }

    @Test
    void findByActivationToken() {
        Optional<Tokens> tokensOptional = tokenRepository.findByActivationToken(tokens.getActivationToken());
        assertNotNull(tokensOptional.get());
        assertEquals(tokens,tokensOptional.get());
    }

    @Test
    void findByUserToken() {
        Optional<Tokens> tokensOptional = tokenRepository.findByUserToken(tokens.getUserToken());
        assertNotNull(tokensOptional.get());
        assertEquals(tokens,tokensOptional.get());
    }
}