package server_monitoring_back.repositories;

import com.mongodb.client.MongoCollection;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import server_monitoring_back.models.Tokens;
import server_monitoring_back.models.User;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.db_service.DBServiceImpl;
import server_monitoring_back.services.token_service.TokenService;
import server_monitoring_back.services.token_service.TokenServiceImpl;

import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryImplIT {

    private static final String PASSWORD_HASH_KEY = System.getenv("PASSWORD_HASH_KEY");

    private final TokenService tokenService = new TokenServiceImpl();

    final DBService dbService = new DBServiceImpl();
    UserRepository userRepository = new UserRepositoryImpl(dbService, "user_info", new User());
    MongoCollection collection;
    User user;
    final String PASSWORD = "pass";

    @BeforeEach
    void setUp() {
        collection = dbService.getCollection( "user_info", new User());
        user = new User();
        user.setPassword(tokenService.dataHashing(PASSWORD,PASSWORD_HASH_KEY));
        user.setUsername("user");
        user.setEmail("email");
        user.setId(new ObjectId());
        collection.insertOne(user);
    }

    @AfterEach
    void tearDown() {
        collection.findOneAndDelete(eq("_id",user.getId()));
    }

    @Test
    void findByUsernameAndPassword() {
        Optional<User> userOptional = userRepository.findByUsernameAndPassword(user.getUsername(),PASSWORD);
        assertNotNull(userOptional.get());
        assertEquals(userOptional.get(),user);
    }


    @Test
    void findByUsernameAndEmail() {
        Optional<User> userOptional = userRepository.findByUsernameAndPassword(user.getUsername(),PASSWORD);
        assertNotNull(userOptional.get());
        assertEquals(userOptional.get(),user);
    }

    @Test
    void findByPassword() {
        Optional<User> userOptional = userRepository.findByPassword(PASSWORD);
        System.out.println(userOptional.get());
        assertNotNull(userOptional.get());
        assertEquals(userOptional.get(),user);
    }

    @Test
    void existByUsername() {
        assertTrue(userRepository.existByUsername(user.getUsername()));
    }
    @Test
    void existByUsernameFail() {
        assertFalse(userRepository.existByUsername("asdfagsdagasdgdsa"));
    }

    @Test
    void existByUsernameOrEmail() {
        assertTrue(userRepository.existByUsernameOrEmail(user.getUsername(),user.getEmail()));
    }
    @Test
    void existByUsernameFalseOrEmail() {
        assertFalse(userRepository.existByUsernameOrEmail("asfafasf","aaasfaf"));
    }


    @Test
    void save() {
        collection.findOneAndDelete(eq("_id",user.getId()));
        userRepository.save(user);
        MongoCollection tokenCollection = dbService.getCollection("user_tokens",new Tokens());
        User resultUser = (User)collection.find(eq("_id",user.getId())).first();
        Tokens resultTokens = (Tokens)tokenCollection.find(eq("user_id",user.getId())).first();
        assertNotNull(resultTokens);
        assertNotNull(resultUser);
        assertEquals(user,resultUser);
        tokenCollection.findOneAndDelete(eq("user_id",user.getId()));
    }

    @Test
    void deleteById() {
        MongoCollection tokenCollection = dbService.getCollection("user_tokens",new Tokens());
        Tokens tokens = new Tokens();
        tokens.setUserId(user.getId());
        tokenCollection.insertOne(tokens);
        userRepository.deleteById(user.getId());
        assertNull(collection.find(eq("_id", user.getId())).first());
        assertNull(tokenCollection.find(eq("user_id", user.getId())).first());
        tokenCollection.findOneAndDelete(eq("user_id",user.getId()));
    }
    @Test
    void deleteByIdFail() {
        MongoCollection tokenCollection = dbService.getCollection("user_tokens",new Tokens());
        Tokens tokens = new Tokens();
        tokens.setUserId(user.getId());
        tokenCollection.insertOne(tokens);
        userRepository.deleteById(new ObjectId());
        assertNotNull(collection.find(eq("_id", user.getId())).first());
        assertNotNull(tokenCollection.find(eq("user_id", user.getId())).first());
        tokenCollection.findOneAndDelete(eq("user_id",user.getId()));
    }

    @Test
    void delete() {
        MongoCollection tokenCollection = dbService.getCollection("user_tokens",new Tokens());
        Tokens tokens = new Tokens();
        tokens.setUserId(user.getId());
        tokenCollection.insertOne(tokens);
        userRepository.delete(user);
        assertNull(collection.find(eq("_id", user.getId())).first());
        assertNull(tokenCollection.find(eq("user_id", user.getId())).first());
        tokenCollection.findOneAndDelete(eq("user_id",user.getId()));
    }

    @Test
    void updatePassword() {
        userRepository.updatePassword("newPass",user);
        User resultUser = (User)collection.find(eq("_id", user.getId())).first();
        assert resultUser != null;
        assertNotEquals(resultUser.getPassword(),user.getPassword());
    }

    @Test
    void setActiveTo() {
        userRepository.setActiveTo(user.getId(),!user.isActive());
        User resultUser = (User)collection.find(eq("_id", user.getId())).first();
        assert resultUser != null;
        assertNotEquals(resultUser.isActive(),user.isActive());
    }

    @Test
    void addUserPic() {
        userRepository.addUserPic(user,"ref");
        User resultUser = (User)collection.find(eq("_id", user.getId())).first();
        assert resultUser != null;
        assertEquals(resultUser.getProfilePic(),"ref");
    }


}