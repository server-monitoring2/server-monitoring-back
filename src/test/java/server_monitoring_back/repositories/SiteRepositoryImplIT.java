package server_monitoring_back.repositories;

import com.mongodb.client.MongoCollection;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import server_monitoring_back.models.Site;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.db_service.DBServiceImpl;

import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;

class SiteRepositoryImplIT {
    final DBService dbService = new DBServiceImpl();
    SiteRepository siteRepository = new SiteRepositoryImpl(dbService, "site_info", new Site());
    MongoCollection collection;
    Site site;

    @BeforeEach
    void setUp() {
        collection = dbService.getCollection("site_info", new Site());
        site = new Site();
        site.setUserId(new ObjectId());
        site.setUrl("url");
        collection.insertOne(site);
    }

    @AfterEach
    void tearDown() {
        collection.findOneAndDelete(eq("user_id",site.getUserId()));
    }

    @Test
    void getSitesByUserId() {
        List<Site> sites = siteRepository.getSitesByUserId(site.getUserId());
        assertEquals(sites.size(),1);
        assertEquals(site,sites.get(0));
    }

    @Test
    void existByUrl() {
        assertTrue(siteRepository.existByUrl(site.getUrl()));
    }
    @Test
    void existByUrlFail() {
        assertFalse(siteRepository.existByUrl("asfafasdfdsafsdfa"));
    }

    @Test
    void getSiteByUrl() {
        Optional<Site> optionalSite = siteRepository.getSiteByUrl(site.getUrl());
        assertEquals(site,optionalSite.get());
    }

    @Test
    void updateSite() {
        Site updatetSite = new Site();
        updatetSite.setOldUrl(site.getUrl());
        updatetSite.setNewUrl("changed");
        siteRepository.updateSite(updatetSite);
        Site resultSite = (Site)collection.find(eq("user_id",site.getUserId())).first();
        assertNotNull(resultSite);
        assertEquals(site,resultSite);
    }

    @Test
    void deleteByUserId() {
        siteRepository.deleteByUserId(site.getUserId());
        assertNull(collection.find(eq("user_id", site.getUserId())).first());
    }
    @Test
    void deleteByUserIdFail() {
        siteRepository.deleteByUserId(new ObjectId());
        assertNotNull(collection.find(eq("user_id", site.getUserId())).first());
    }
}