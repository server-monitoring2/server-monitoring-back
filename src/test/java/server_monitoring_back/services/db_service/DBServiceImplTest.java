package server_monitoring_back.services.db_service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class DBServiceImplTest {
    @Mock
    private MongoDatabase database;
    @Mock
    private MongoClient mongoClients;
    @InjectMocks
    DBService dbService= new DBServiceImpl();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void getCollection() {
        MongoCollection mongoCollection = null;
        when(database.getCollection(anyString(),any())).thenReturn(mongoCollection);
        assertEquals(mongoCollection,dbService.getCollection("test",new Object()));
    }
}