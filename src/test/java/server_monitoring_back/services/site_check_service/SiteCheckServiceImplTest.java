package server_monitoring_back.services.site_check_service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import server_monitoring_back.models.Site;
import server_monitoring_back.models.SiteLog;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class SiteCheckServiceImplTest {
    private final SiteCheckService siteCheckService = new SiteCheckServiceImpl();
    SiteLog site = new SiteLog();
    String url = "https://google.com";

    @BeforeEach
    void setUp() {

        MockitoAnnotations.openMocks(this);

    }



    @Test
    void checkIfAvailable() {
        URL url1 = null;
        try {
            site.setCode(0);
            url1 = new URL(url);
            HttpURLConnection connections = (HttpURLConnection)url1.openConnection();
            int code = connections.getResponseCode();
            System.out.println("aa" + code);
            site.setCode(code);
            site.setAvailable(site.getCode() == 200);
            System.out.println(site.getCode());
        } catch (IOException e) {
            e.printStackTrace();
        }
        SiteLog siteLog = new SiteLog();
        siteCheckService.checkIfAvailable(url,siteLog);
        assertEquals(site.getCode(),siteLog.getCode());
        assertEquals(site.getAvailable(),siteLog.getAvailable());
    }

    @Test
    void checkIfAvailableFalse() {
            site.setCode(124);
        SiteLog siteLog = new SiteLog();
        siteCheckService.checkIfAvailable(url,siteLog);
        assertNotEquals(site.getCode(),siteLog.getCode());
        assertNotEquals(site.getAvailable(),siteLog.getAvailable());
    }
}