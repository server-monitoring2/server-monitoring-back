package server_monitoring_back.services.agent_service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import server_monitoring_back.services.token_service.TokenService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class AgentServiceImplTest {
    @Mock
    TokenService tokenService;
    @InjectMocks
    AgentService agentService = new AgentServiceImpl();



    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void validateAgent() {
        String hashResult = "hashedData";
        String sign = "hashedData";
        when(tokenService.dataHashing(anyString(),anyString())).thenReturn(hashResult);
        assertTrue(agentService.validateAgent("test", sign,"asdf"));
    }
}