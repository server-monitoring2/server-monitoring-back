package server_monitoring_back.services.token_service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class TokenServiceImplTest {

    TokenService tokenService = new TokenServiceImpl();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void generateToken() {
        Random random = new Random();
        int leftLimit = 48;
        int rightLimit = 122;
        int targetStringLength = 10;
        String generatedToken = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        assertNotEquals(generatedToken,tokenService.generateToken());
    }

    @Test
    void dataHashing() {
        String data = "data";
        String key = "key";
        byte[] hmacSha256 = null;
        StringBuilder result = new StringBuilder();
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(data.getBytes());
            for (final byte element : hmacSha256) {
                result.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
            }
            System.out.println("Result:[" + result + "]");
        } catch (Exception e) {
        }
        assertEquals(result.toString(),tokenService.dataHashing(data,key));
    }
}