package server_monitoring_back.services.validation_service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import server_monitoring_back.models.SiteLog;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValidationServiceImplTest {

    private ValidationService validationService = new ValidationServiceImpl();

    List<SiteLog> siteLogs = new ArrayList<>();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void validateSiteLogs() {
        assertEquals("{\"error\": \"no logs for now\"}",validationService.validateSiteLogs(siteLogs));

    }
}