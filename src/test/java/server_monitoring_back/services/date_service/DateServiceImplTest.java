package server_monitoring_back.services.date_service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DateServiceImplTest {
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private Calendar calendar;

    DateService dateService = new DateServiceImpl();

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetCurrentDateString() {
        calendar = Calendar.getInstance();
        assertEquals( dateFormat.format(calendar.getTime()),dateService.getCurrentDateString());
    }

    @Test
    void testGetCurrentDate() {
        calendar = Calendar.getInstance();
        assertEquals( String.valueOf(calendar.getTimeInMillis()),dateService.getCurrentDate());
    }

    @Test
    void testDateDifference() {
        assertEquals( 0,dateService.dateDifference(new Date(),new Date()));
    }
}