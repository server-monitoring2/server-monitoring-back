package server_monitoring_back;

import io.javalin.Javalin;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import server_monitoring_back.controllers.AgentController;
import server_monitoring_back.controllers.UserController;
import server_monitoring_back.models.AgentLog;

import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AppTest {
    @Mock
    AgentController agentController = new AgentController();
    @Mock
    Context ctx;


    @BeforeAll

    void init(){
        Javalin app = Javalin.create();
        app.start(123);
        app.routes(() -> {
            path("/agent", () -> {
                path("/log", () -> {
                    post(AgentController.addAgentLogs);
                });
            });
        });
    }


    @BeforeEach
    void setUp() {
    }

    @Test
    public void addAgentLogsSucc(){
        HttpResponse response = Unirest.post("http://localhost:123/agent/log").asString();
        System.out.println(response.getStatusText());
    }
}