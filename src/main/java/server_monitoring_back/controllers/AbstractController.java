package server_monitoring_back.controllers;

import io.javalin.http.Handler;
import server_monitoring_back.repositories.*;
import server_monitoring_back.services.agent_service.AgentService;
import server_monitoring_back.services.agent_service.AgentServiceImpl;
import server_monitoring_back.services.date_service.DateService;
import server_monitoring_back.services.date_service.DateServiceImpl;
import server_monitoring_back.services.email_service.EmailService;
import server_monitoring_back.services.email_service.EmailServiceImpl;
import server_monitoring_back.services.pic_service.PicService;
import server_monitoring_back.services.pic_service.PicServiceImpl;
import server_monitoring_back.services.token_service.TokenService;
import server_monitoring_back.services.token_service.TokenServiceImpl;
import server_monitoring_back.services.validation_service.ValidationService;
import server_monitoring_back.services.validation_service.ValidationServiceImpl;

import java.util.Locale;
import java.util.ResourceBundle;

public abstract class AbstractController {
    private static final Locale defaultLoc = new Locale("en");
    private static Locale loc;
    protected static ResourceBundle resourceBundle;

    protected static final AgentLogRepository agentLogRepository = new AgentLogRepositoryImp();
    protected static final ServerRepository serverRepository = new ServerRepositoryImpl();
    protected static final TokenRepository tokenRepository = new TokensRepositoryImpl();
    protected static final UserRepository userRepository = new UserRepositoryImpl();
    protected final static AgentService agentService = new AgentServiceImpl();
    protected static final EmailService emailService = new EmailServiceImpl();
    protected static final ValidationService validationService = new ValidationServiceImpl();
    protected static final TokenService tokenService = new TokenServiceImpl();
    protected static final PicService picService = new PicServiceImpl();
    protected static final DateService dateservice = new DateServiceImpl();
    protected static final SiteRepository siteRepository = new SiteRepositoryImpl();
    protected static final SiteLogRepository siteLogRepository = new SiteLogRepositoryImpl();
    protected static final UserPaymentRepository userPaymentRepository = new UserPaymentRepositoryImpl();

    protected static final int USER_PREMIUM_PRICE = 500;
    protected static final String PASSWORD_HASH_KEY = System.getenv("PASSWORD_HASH_KEY");

    public static Handler localizationSetting = ctx -> {
        System.out.println("sa");
        Locale.setDefault(defaultLoc);
        String lang =ctx.header("Accept-Language");
        if (lang != null) {
            loc = new Locale(lang);
        }else loc = defaultLoc;
        resourceBundle = ResourceBundle.getBundle("i18n",loc);
    };
}
