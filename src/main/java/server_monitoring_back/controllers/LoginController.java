package server_monitoring_back.controllers;

import io.javalin.http.Handler;
import server_monitoring_back.models.User;
import java.util.Optional;

public class LoginController extends AbstractController{



    public static Handler userLogin = ctx -> {
        User user = ctx.bodyAsClass(User.class);
        Optional<User> optionalUser = userRepository.findByUsernameAndPassword(user.getUsername()
                , user.getPassword());
        if (optionalUser.isPresent()) {
            if (optionalUser.get().isActive()) {
                if(tokenRepository.findByUserId(optionalUser.get()
                        .getId()).isPresent()){
                    ctx.result("{\"token\": \"" + tokenRepository.findByUserId(optionalUser.get()
                            .getId())
                            .get()
                            .getUserToken() + "\"}");
                    ctx.status(200);
                }
            } else {
                ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                ctx.status(412);
            }
        } else {
            ctx.result(resourceBundle.getString("LOGIN_FAIL"));
            ctx.status(403);
        }
    };
}
