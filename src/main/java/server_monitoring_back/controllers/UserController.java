package server_monitoring_back.controllers;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import server_monitoring_back.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



@NoArgsConstructor
public class UserController extends AbstractController {

    protected static Optional<User> optionalUser = Optional.empty();
    protected static String responce;



    public static void checkToken(Context ctx) {
        String token = ctx.header("Authorization");
        optionalUser = Optional.empty();
        tokenRepository.findByUserToken(token)
                .ifPresentOrElse(tokens -> {
                    optionalUser = userRepository.findById(tokens.getUserId());
                    System.out.println(optionalUser.get().toString());
                }, () -> {
                    responce = resourceBundle.getString("AUTHORIZATION_FAIL");
                });
    }

    protected static PasswordChange passwordChange;

    public static Handler activateUser = ctx -> {
        String activationToken = ctx.pathParam("activationToken");
        Optional<Tokens> tokensOptional = tokenRepository.findByActivationToken(activationToken);
        if (tokensOptional.isPresent()) {
            Optional<User> userOptional = userRepository.findById(tokensOptional.get().getUserId());
            if (!userOptional.get().isActive()) {
                userRepository.setActiveTo(tokensOptional.get().getUserId(), true);
                ctx.result(resourceBundle.getString("USER_ACTIVATED"));
                ctx.status(201);
            } else {
                ctx.result(resourceBundle.getString("USER_ACTIVATION_FAIL"));
                ctx.status(400);
            }

        } else {
            ctx.result(resourceBundle.getString("AUTHORIZATION_FAIL"));
            ctx.status(400);
        }
    };
    public static Handler getAgentData = ctx -> {
        optionalUser.ifPresentOrElse(user -> {
            ServerDates serverDates = ctx.bodyAsClass(ServerDates.class);
            String publicKey = ctx.pathParam("serverValue");
            serverRepository.getServerByPublicKey(publicKey).ifPresentOrElse(server -> {
                if(server.getUserId().equals(user.getId())){
                    List<AgentLog> agentLogList = agentLogRepository.findLogsByPublicKeyAndSortByDates(publicKey
                            ,serverDates.getFirstDate(),serverDates.getSecondDate());
                    List<AgentLog> agentLogs = new ArrayList<>();
                    for(int i =0 ; i < agentLogList.size(); i+=60){
                        agentLogs.add(agentLogList.get(i));
                    }
                    ctx.json(agentLogs);
                    ctx.status(200);
                }else {
                    ctx.result(resourceBundle.getString("SERVER_DELETING_FAIL"));
                    ctx.status(400);
                }

            },()->{
                ctx.result(resourceBundle.getString("SERVER_DELETING_FAIL"));
                ctx.status(400);
            });
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });

    };
    public static Handler deleteUser = ctx -> {
        if (optionalUser.isPresent()) {
            userRepository.deleteById(optionalUser.get().getId());
            ctx.result(resourceBundle.getString("SUCCESS"));
            ctx.status(204);
        } else {
            ctx.result(resourceBundle.getString("AUTHORIZATION_FAIL"));
            ctx.status(403);
        }
    };
    public static Handler getUserStatus = ctx -> {
        optionalUser.ifPresentOrElse(user -> {
            ctx.result("{\"username\": \"" + user.getUsername()
                    + "\",\"is_premium\": \"" + user.isPremium() + "\"}");
            ctx.status(200);
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });
    };

    public static Handler changeUserPassword = ctx -> {
        optionalUser.ifPresentOrElse(user1 -> {
            if (user1.isActive()) {
                String hashedPassword = tokenService.dataHashing(passwordChange.getNewPassword(),
                        PASSWORD_HASH_KEY);
                userRepository.updatePassword(hashedPassword, user1);
                ctx.result(resourceBundle.getString("SUCCESS"));
                ctx.status(200);
            } else {
                ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                ctx.status(412);
            }
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });
    };

    public static void checkTokenAndPassword(Context ctx) {
        String token = ctx.header("Authorization");
        passwordChange = ctx.bodyAsClass(PasswordChange.class);
        String hashedPassword = tokenService.dataHashing(passwordChange.getOldPassword(),
                PASSWORD_HASH_KEY);
        optionalUser = Optional.empty();
        tokenRepository.findByUserToken(token)
                .ifPresentOrElse(tokens -> {
                    if (!hashedPassword.equals(userRepository.findById(tokens.getUserId()).get().getPassword())) {
                        responce = resourceBundle.getString("PASSWORD_CHANGING_FAIL");
                    } else {
                        optionalUser = userRepository.findById(tokens.getUserId());
                    }
                }, () -> {
                    responce = resourceBundle.getString("AUTHORIZATION_FAIL");
                });
    }
}
