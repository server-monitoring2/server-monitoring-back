package server_monitoring_back.controllers;


import io.javalin.http.Context;
import io.javalin.http.Handler;
import server_monitoring_back.models.Server;
import server_monitoring_back.models.ServerDates;

import java.util.List;
import java.util.function.BiConsumer;

public class ServerController extends UserController {

    public static Handler changeServer = ctx -> {
        Server server = ctx.bodyAsClass(Server.class);
        optionalUser.ifPresentOrElse(user -> {
            if (user.isActive()) {

            } else {
                ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                ctx.status(412);
            }
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });
    };

    public static Handler getServers = ctx -> {
        optionalUser.ifPresentOrElse(user -> {
                    List<Server> serverList = serverRepository.getServersByUserId(optionalUser.get().getId());
                    ctx.json(serverList);
                    ctx.status(200);
                }
                , () -> {
                    ctx.result(responce);
                    ctx.status(400);
                });

    };
    public static Handler deleteServer = ctx -> {
        String publicKey = ctx.pathParam("publicKey");
        optionalUser.ifPresentOrElse(user -> {
                    if (user.isActive()) {
                        serverRepository.getServerByPublicKey(publicKey).ifPresentOrElse(server -> {
                            serverRepository.delete(server);
                            agentLogRepository.deleteByPublicKey(publicKey);
                            ctx.result(resourceBundle.getString("SUCCESS"));
                            ctx.status(200);
                        }, () -> {
                            ctx.result(resourceBundle.getString("SERVER_DELETING_FAIL"));
                            ctx.status(412);
                        });
                    } else {
                        ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                        ctx.status(412);
                    }
                }
                , () -> {
                    ctx.result(responce);
                    ctx.status(400);
                });
    };


    public static void serverUpdate(Context ctx, BiConsumer<Context, Server> methodName) {
        Server server = ctx.bodyAsClass(Server.class);
        optionalUser.ifPresentOrElse(user -> {
            if (!serverRepository.existByPublicKey(server.getPublicKey())) {
                if (user.isActive()) {
                    server.setUserId(user.getId());
                    methodName.accept(ctx, server);
                } else {
                    ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                    ctx.status(412);
                }
            } else {
                ctx.result(resourceBundle.getString("SERVER_ADDING_FAIL"));
                ctx.status(403);
            }
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });
    }

    public static void addServer(Context ctx, Server server) {
        if (optionalUser.get().isPremium()) {
            serverRepository.save(server);
            ctx.result(resourceBundle.getString("SUCCESS"));
            ctx.status(200);
        } else {
            if (serverRepository.getServersByUserId(server.getUserId()).size() >= 2) {
                ctx.result(resourceBundle.getString("SERVER_ADDING_USER_NOT_PREMIUM"));
                ctx.status(412);
            } else {
                serverRepository.save(server);
                ctx.result(resourceBundle.getString("SUCCESS"));
                ctx.status(200);
            }
        }
    }

    public static void changeServer(Context ctx, Server server) {
        if (serverRepository.existByPublicKey(server.getOldPublicKey())) {
            if (!serverRepository.existByPublicKey(server.getNewPublicKey())) {
                serverRepository.updateServer(server);
                ctx.result(resourceBundle.getString("SUCCESS"));
                ctx.status(200);
            } else {
                ctx.result(resourceBundle.getString("SERVER_ADDING_FAIL"));
                ctx.status(412);
            }
        } else {
            ctx.result(resourceBundle.getString("SERVER_CHANGING_FAIL"));
            ctx.status(412);
        }
    }
}
