package server_monitoring_back.controllers;

import com.google.gson.JsonSyntaxException;
import com.stripe.exception.StripeException;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import com.stripe.model.PaymentIntent;
import com.stripe.model.StripeObject;
import com.stripe.net.ApiResource;
import com.stripe.param.PaymentIntentCreateParams;
import io.javalin.http.Handler;
import server_monitoring_back.models.CreatePaymentResponse;
import server_monitoring_back.models.User;
import server_monitoring_back.models.UserPremiumPayment;

import java.util.Optional;


public class StripeController extends UserController{



    public static Handler stripeWebHook = ctx -> {
        String payload = ctx.body();
        Event event = null;
        try {
            event = ApiResource.GSON.fromJson(payload, Event.class);
        } catch (JsonSyntaxException e) {
            ctx.status(400);
            return;
        }

        EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
        StripeObject stripeObject = null;
        if (dataObjectDeserializer.getObject().isPresent()) {
            stripeObject = dataObjectDeserializer.getObject().get();
        } else {
            ctx.status(400);
            return;
        }
        switch (event.getType()) {
            case "payment_intent.succeeded":
                PaymentIntent paymentIntent = (PaymentIntent) stripeObject;
                Optional<UserPremiumPayment> userPremiumPayment = userPaymentRepository
                        .findUserPaymentByClientSecret(paymentIntent.getClientSecret());
                userPremiumPayment.ifPresent(user -> {
                    Optional<User> user1 = userRepository.findById(user.getUserId());
                    user1.ifPresent(user2 -> {
                        userPaymentRepository.changeStatus(user2);
                        userRepository.activateUser(user2);
                    });

                });
                break;
            case "payment_intent.payment_failed":
                break;
            case "payment_intent.canceled":
                break;
            default:
                System.out.println("Unhandled event type: " + event.getType());
        }

        System.out.print(payload);
        ctx.status(200);

    };

    public static Handler paymentHandler = ctx -> {
        optionalUser.ifPresentOrElse(user -> {
            if(user.isActive()){
                if(!user.isPremium()){
                    PaymentIntentCreateParams createParams = new PaymentIntentCreateParams.Builder().setCurrency("usd")
                            .setAmount((long) USER_PREMIUM_PRICE).build();
                    PaymentIntent intent = null;
                    try {
                        intent = PaymentIntent.create(createParams);
                        String clientSecret = intent.getClientSecret();
                        CreatePaymentResponse paymentResponse = new CreatePaymentResponse(clientSecret);
                        UserPremiumPayment userPremiumPayment = new UserPremiumPayment(user.getId()
                                ,clientSecret,false);
                        userPaymentRepository.save(userPremiumPayment);
                        ctx.result(paymentResponse.getCreatePaymentResponse());
                        ctx.status(200);
                    } catch (StripeException e) {
                        e.printStackTrace();
                        ctx.status(500);
                    }
                }else{
                    ctx.result(resourceBundle.getString("USER_IS_PREMIUM"));
                    ctx.status(412);
                }
            }else{
                ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                ctx.status(412);
            }
        },()->{
            ctx.result(responce);
            ctx.status(400);
        });

    };
}
