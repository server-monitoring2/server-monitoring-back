package server_monitoring_back.controllers;

import io.javalin.http.Handler;
import io.javalin.http.UploadedFile;

public class PicController extends UserController {


    public static Handler uploadPic = ctx -> {
        UploadedFile uploadedFile = ctx.uploadedFile("photo");
        if(uploadedFile == null || uploadedFile.getSize() == 0){
            ctx.result(resourceBundle.getString("PIC_ADDING_FAIL"));
            ctx.status(400);
        }else {
            String token = ctx.header("Authorization");
            optionalUser.ifPresentOrElse(user1 -> {
                if (user1.isActive()) {
                    String fileName = token
                            + dateservice.getCurrentDate() + uploadedFile.getFilename();
                    if (picService.validatePic(uploadedFile, token, fileName)) {
                        if (picService.deleteLastPic(user1.getProfilePic())) {
                            userRepository.addUserPic(user1, fileName);
                            ctx.result(resourceBundle.getString("SUCCESS"));
                            ctx.status(200);
                        } else {
                            ctx.result(resourceBundle.getString("PIC_DELETING_FAIL"));
                            ctx.status(400);
                        }
                    } else {
                        ctx.result(resourceBundle.getString("USER_PICTURE_FORMAT"));
                        ctx.status(404);
                    }
                } else {
                    ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                    ctx.status(412);
                }
            }, () -> {
                ctx.result(responce);
                ctx.status(400);
            });
        }
    };
    public static Handler getPic = ctx -> {
        optionalUser.ifPresentOrElse(user1 -> {
            if (user1.isActive()) {
                if(user1.getProfilePic() != null){
                    ctx.result("{\"pic\": \"api.t2.tss2020.site/media/"
                            + user1.getProfilePic()+"\"}");
                    ctx.status(200);
                }else{
                    ctx.result(resourceBundle.getString("USER_PICTURE_FAIL"));
                    ctx.status(400);
                }

            } else {
                ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                ctx.status(412);
            }
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });
    };
}
