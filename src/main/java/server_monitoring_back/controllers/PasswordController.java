package server_monitoring_back.controllers;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import server_monitoring_back.models.Tokens;
import server_monitoring_back.models.User;
import java.util.Optional;

public class PasswordController extends AbstractController{



    private static Optional<User> optionalUser;
    private static boolean checked = false;
    private static boolean active = false;

    public static void checkUser(Context ctx){
        User user = ctx.bodyAsClass(User.class);
        optionalUser = userRepository.findByUsernameAndEmail(user.getUsername(),user.getEmail());
        if(optionalUser.isPresent()){
            checked = true;
            active = optionalUser.get().isActive();
        }else {
            checked = false;
            active = false;
        }
    }

    public static Handler sendResetPasswordEmail = ctx -> {
        if(checked){
            if(active){
                    emailService.sendMail(tokenRepository.findByUserId(optionalUser.get().getId()).get().getPasswordResetToken()
                            ,optionalUser.get().getEmail(), resourceBundle.getString("ACCOUNT_PASSWORD_RESET"),
                            resourceBundle.getString("PASSWORD_RESET_SUBJECT"));
                ctx.result(resourceBundle.getString("SUCCESS"));
                ctx.status(200);
            }else{
                ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                ctx.status(412);
            }
        }else{
            ctx.result(resourceBundle.getString("LOGIN_FAIL"));
            ctx.status(400);
        }
    };

    public static Handler resetPassword = ctx -> {
        String reseting = ctx.pathParam("resetPasswordToken");
        Optional<Tokens> tokensOptional = tokenRepository.findByResetToken(reseting);
        if(tokensOptional.isPresent()){
            String newPassword = tokenService.generateToken();
            userRepository.updatePassword(tokenService.dataHashing(newPassword,PASSWORD_HASH_KEY)
                    ,userRepository.findById(tokensOptional.get().getUserId()).get());
            tokenRepository.setNewResetToken(tokensOptional.get());
                emailService.sendMail(newPassword,userRepository.findById(tokensOptional
                        .get().getUserId()).get().getEmail(),resourceBundle.getString("ACCOUNT_NEW_PASSWORD"),
                        resourceBundle.getString("NEW_PASSWORD_SUBJECT"));
            ctx.result(resourceBundle.getString("SUCCESS"));
            ctx.status(200);
        }else {
            ctx.result(resourceBundle.getString("PASSWORD_RESET_ERROR"));
            ctx.status(412);
        }
    };
}
