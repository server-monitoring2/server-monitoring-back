package server_monitoring_back.controllers;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import server_monitoring_back.models.AgentLog;
import server_monitoring_back.models.Server;
import server_monitoring_back.models.Site;
import server_monitoring_back.models.SiteLog;

import java.util.List;
import java.util.function.BiConsumer;

public class SiteController extends UserController {


    public static Handler deleteServer = ctx -> {
        String url = ctx.url();
        String userSite = url.split("/user/url/delete/")[1];
        optionalUser.ifPresentOrElse(user -> {
                    if (user.isActive()) {
                        siteRepository.getSiteByUrl(userSite).ifPresentOrElse(site -> {
                            siteRepository.delete(site);
                            ctx.result(resourceBundle.getString("SUCCESS"));
                            ctx.status(200);
                        }, () -> {
                            ctx.result(resourceBundle.getString("SITE_DELETING_FAIL"));
                            ctx.status(412);
                        });
                    } else {
                        ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                        ctx.status(412);
                    }
                }
                , () -> {
                    ctx.result(responce);
                    ctx.status(400);
                });
    };
    public static Handler getSites = ctx -> {
        optionalUser.ifPresentOrElse(user -> {
                    List<Site> siteList = siteRepository.getSitesByUserId(optionalUser.get().getId());
                    ctx.json(siteList);
                    ctx.status(200);
                }
                , () -> {
                    ctx.result(responce);
                    ctx.status(400);
                });
    };
    public static Handler getSiteLog = ctx -> {
        String url = ctx.url();
        String userSite = url.split("/user/site/list/")[1];
        String token = ctx.header("Authorization");
        tokenRepository.findByUserToken(token).ifPresentOrElse(tokens -> {
            List<SiteLog> siteLogs = siteLogRepository.findLogsByUrlAndUserId(userSite, tokens.getUserId());
            responce = validationService.validateSiteLogs(siteLogs);
            ctx.result(responce);
            ctx.status(200);
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });
    };


    public static Handler addSite= ctx ->  {
        Site site = ctx.bodyAsClass(Site.class);
        optionalUser.ifPresentOrElse(user -> {
            if (user.isActive()) {
                site.setUserId(user.getId());
                site.setAt(dateservice.getCurrentDateString());
                if (siteRepository.existByUrlAndUserId(site.getUrl(),user.getId())) {
                    ctx.result(resourceBundle.getString("SITE_ADDING_NAME_FAIL"));
                    ctx.status(412);
                } else if (optionalUser.get().isPremium()) {
                    if (siteRepository.getSitesByUserId(site.getUserId()).size() < 10) {
                        siteRepository.save(site);
                        ctx.result(resourceBundle.getString("SUCCESS"));
                        ctx.status(200);
                    } else {
                        ctx.result(resourceBundle.getString("SITE_ADDING_USER_PREMIUM"));
                        ctx.status(412);
                    }
                } else {
                    if (siteRepository.getSitesByUserId(site.getUserId()).size() >= 3) {
                        ctx.result(resourceBundle.getString("SITE_ADDING_USER_NOT_PREMIUM"));
                        ctx.status(412);
                    } else {
                        siteRepository.save(site);
                        ctx.result(resourceBundle.getString("SUCCESS"));
                        ctx.status(200);
                    }
                }
            } else {
                ctx.result(resourceBundle.getString("USER_NOT_ACTIVE"));
                ctx.status(412);
            }
        }, () -> {
            ctx.result(responce);
            ctx.status(400);
        });

    };
}
