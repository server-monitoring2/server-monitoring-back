package server_monitoring_back.controllers;


import io.javalin.http.Context;
import io.javalin.http.Handler;
import server_monitoring_back.App;
import server_monitoring_back.models.Tokens;
import server_monitoring_back.models.User;
import server_monitoring_back.repositories.TokenRepository;
import server_monitoring_back.repositories.TokensRepositoryImpl;
import server_monitoring_back.repositories.UserRepository;
import server_monitoring_back.repositories.UserRepositoryImpl;
import server_monitoring_back.services.date_service.DateService;
import server_monitoring_back.services.date_service.DateServiceImpl;
import server_monitoring_back.services.email_service.EmailService;
import server_monitoring_back.services.email_service.EmailServiceImpl;
import server_monitoring_back.services.token_service.TokenService;
import server_monitoring_back.services.token_service.TokenServiceImpl;

import java.util.Optional;

public class RegisterController extends AbstractController{
    private static User user;
    private static Optional<Tokens> tokens;
    private static boolean checked;
    private static boolean registered;



    public static void checkIfRegistered(Context ctx){
        user = ctx.bodyAsClass(User.class);
        System.out.println(user.toString());
        checked = userRepository.existByUsernameOrEmail(user.getUsername(), user.getEmail());
        System.out.println(checked);
    };

    public static Handler userRegister = ctx -> {
        if(checked){
            ctx.result(resourceBundle.getString("REGISTRATION_FAIL"));
            ctx.status(400);
        }else{
            user.setPassword(tokenService.dataHashing(user.getPassword(),PASSWORD_HASH_KEY));
            user.setDateOfRegistration(dateservice.getCurrentDateString());
            userRepository.save(user);
            registered = true;
            ctx.result(resourceBundle.getString("SUCCESS"));
            ctx.status(201);
        }
    };
    public static void sendActivationEmail(Context ctx){
        tokens = tokenRepository.findByUserId(user.getId());
        if(registered){
            tokens.ifPresent(value -> emailService.sendMail(value.getActivationToken()
                    , user.getEmail(), resourceBundle.getString("ACCOUNT_ACTIVATION_LINK"),
                    resourceBundle.getString("ACTIVATION_LINK_SUBJECT")));
        }
    };
}
