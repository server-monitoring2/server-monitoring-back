package server_monitoring_back.controllers;

import io.javalin.http.Handler;
import server_monitoring_back.models.AgentLog;


public class AgentController extends AbstractController{

    public static Handler addAgentLogs = ctx -> {
        String sign = ctx.header("Sign");
        String body = ctx.body();
        AgentLog agentLog = ctx.bodyAsClass(AgentLog.class);
        serverRepository.getServerByPublicKey(agentLog.getPublicKey()).ifPresentOrElse(server -> {
            if (agentService.validateAgent(body, sign, server.getPrivateKey())) {
                agentLogRepository.save(agentLog);
                ctx.status(201);
            } else {
                ctx.result(resourceBundle.getString("AGENT_DATA_FAIL"));
                ctx.status(412);
            }
        }, () -> {
            ctx.status(400);
        });

    };

}
