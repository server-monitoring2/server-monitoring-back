package server_monitoring_back.models;


import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CreatePaymentResponse {
    private String clientSecret;

    public String getCreatePaymentResponse() {
        return "{\"clientSecret\":\"" + clientSecret + "\"}";
      }
}
