package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;
import server_monitoring_back.models.inner_agent_classes.CPU;
import server_monitoring_back.models.inner_agent_classes.Disks;
import server_monitoring_back.models.inner_agent_classes.Memory;
import server_monitoring_back.models.inner_agent_classes.Network;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AgentLog extends BaseEntity{
    @JsonProperty(value = "host")
    @BsonProperty(value = "host")
    private String host;
    @JsonProperty(value = "at")
    @BsonProperty(value = "at")
    private String at;
    @JsonProperty(value = "boot_time")
    @BsonProperty(value = "boot_time")
    private String bootTime;
    @JsonProperty(value = "public_key")
    @BsonProperty(value = "public_key")
    private String publicKey;
    @JsonProperty(value = "agent_version")
    @BsonProperty(value = "agent_version")
    private String agentVersion;
    @JsonProperty(value = "cpu")
    @BsonProperty(value = "cpu")
    private List<CPU> cpu;
    @JsonProperty(value = "disks")
    @BsonProperty(value = "disks")
    private List<Disks> disks;
    @JsonProperty(value = "load")
    @BsonProperty(value = "load")
    private List<Float> load;
    @JsonProperty(value = "memory")
    @BsonProperty(value = "memory")
    private Memory memory;
    @JsonProperty(value = "network")
    @BsonProperty(value = "network")
    private List<Network> network;
}
