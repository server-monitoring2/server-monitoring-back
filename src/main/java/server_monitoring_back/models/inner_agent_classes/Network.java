package server_monitoring_back.models.inner_agent_classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Network {
    @JsonProperty(value = "name")
    @BsonProperty(value = "name")
    public String name;
    @JsonProperty(value = "in")
    @BsonProperty(value = "in")
    public long in;
    @JsonProperty(value = "out")
    @BsonProperty(value = "out")
    public long out;
}
