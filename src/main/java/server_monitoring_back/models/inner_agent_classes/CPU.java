package server_monitoring_back.models.inner_agent_classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class CPU {
    @JsonProperty(value = "num")
    @BsonProperty(value = "num")
    public Float num;
    @JsonProperty(value = "user")
    @BsonProperty(value = "user")
    public Float user;
    @JsonProperty(value = "system")
    @BsonProperty(value = "system")
    public Float system;
    @JsonProperty(value = "idle")
    @BsonProperty(value = "idle")
    public Float idle;
}
