package server_monitoring_back.models.inner_agent_classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Disks {
    @JsonProperty(value = "origin")
    @BsonProperty(value = "origin")
    public String origin;
    @JsonProperty(value = "free")
    @BsonProperty(value = "free")
    public long free;
    @JsonProperty(value = "total")
    @BsonProperty(value = "total")
    public long total;
}
