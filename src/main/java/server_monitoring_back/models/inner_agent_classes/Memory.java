package server_monitoring_back.models.inner_agent_classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Memory {
    @JsonProperty(value = "wired")
    @BsonProperty(value = "wired")
    public long wired;
    @BsonProperty(value = "free")
    @JsonProperty(value = "free")
    public long free;
    @JsonProperty(value = "active")
    @BsonProperty(value = "active")
    public long active;
    @JsonProperty(value = "inactive")
    @BsonProperty(value = "inactive")
    public long inactive;
    @JsonProperty(value = "total")
    @BsonProperty(value = "total")
    public long total;
}
