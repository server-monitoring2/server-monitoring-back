package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class UserPremiumPayment extends BaseEntity{
    @JsonProperty(value = "user_id")
    @BsonProperty(value = "user_id")
    private ObjectId userId;
    @JsonProperty(value = "client_secret")
    @BsonProperty(value = "client_secret")
    private String clientSecret;
    @JsonProperty(value = "status")
    @BsonProperty(value = "status")
    private boolean status;
}
