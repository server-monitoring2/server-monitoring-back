package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Site extends BaseEntity{
    @JsonProperty(value = "old_url")
    @BsonIgnore
    private String oldUrl;
    @JsonProperty(value = "new_url")
    @BsonIgnore
    private String newUrl;
    @JsonProperty(value = "url")
    @BsonIgnore
    private String Url;
    @JsonIgnore
    @BsonProperty(value = "user_id")
    private ObjectId userId;
    @JsonProperty(value = "at")
    @BsonProperty(value = "at")
    private String at;
}
