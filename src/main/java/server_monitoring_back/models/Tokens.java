package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;
import server_monitoring_back.services.token_service.TokenService;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tokens extends BaseEntity{
    @JsonProperty(value = "user_id")
    @BsonProperty(value = "user_id")
    private ObjectId userId;
    @JsonProperty(value = "user_token")
    @BsonProperty(value = "user_token")
    private String userToken;
    @JsonProperty(value = "password_reset_token")
    @BsonProperty(value = "password_reset_token")
    private String passwordResetToken;
    @JsonProperty(value = "activation_token")
    @BsonProperty(value = "activation_token")
    private String activationToken;

    public Tokens(TokenService tokenService,ObjectId userId) {
        this.userId = userId;
        this.userToken = tokenService.generateToken();
        this.passwordResetToken = tokenService.generateToken();
        this.activationToken = tokenService.generateToken();
    }
}
