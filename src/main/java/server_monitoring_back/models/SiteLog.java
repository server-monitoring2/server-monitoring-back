package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SiteLog extends BaseEntity{
    @JsonProperty(value = "available")
    @BsonProperty(value = "available")
    private Boolean available;
    @JsonProperty(value = "url")
    @BsonProperty(value = "url")
    private String url;
    @JsonProperty(value = "user_id")
    @BsonProperty(value = "user_id")
    private ObjectId userId;
    @JsonProperty(value = "at")
    @BsonProperty(value = "at")
    private String at;
    @JsonProperty(value = "code")
    @BsonProperty(value = "code")
    private Integer code;
}
