package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;

@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
public class PasswordChange {
    @JsonProperty(value = "new_password")
    @BsonProperty(value = "new_password")
    private String newPassword;
    @JsonProperty(value = "old_password")
    @BsonProperty(value = "new_password")
    private String oldPassword;

}
