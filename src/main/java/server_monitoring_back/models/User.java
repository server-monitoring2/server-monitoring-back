package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
@EqualsAndHashCode(callSuper = false)
public class User extends BaseEntity {
    @JsonProperty(value = "username")
    @BsonProperty(value = "username")
    private String username;
    @JsonProperty(value = "password")
    @BsonProperty(value = "password")
    private String password;
    @JsonProperty(value = "email")
    @BsonProperty(value = "email")
    private String email;
    @JsonProperty(value = "active")
    @BsonProperty(value = "active")
    private boolean active;
    @JsonProperty(value = "premium")
    @BsonProperty(value = "premium")
    private boolean premium;
    @JsonProperty(value = "date_of_registration")
    @BsonProperty(value = "date_of_registration")
    private String dateOfRegistration;
    @JsonProperty(value = "profile_pic")
    @BsonProperty(value = "profile_pic")
    private String profilePic;

    public User(ObjectId id, String username, String password, String email, boolean active
            , boolean premium, String dateOfRegistration, String profilePic) {
        super(id);
        this.username = username;
        this.password = password;
        this.email = email;
        this.active = active;
        this.premium = premium;
        this.dateOfRegistration = dateOfRegistration;
        this.profilePic = profilePic;
    }

}
