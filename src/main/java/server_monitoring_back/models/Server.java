package server_monitoring_back.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Server extends BaseEntity {
    @JsonProperty(value = "old_public_key")
    @BsonIgnore
    private String oldPublicKey;
    @JsonProperty(value = "new_public_key")
    @BsonIgnore
    private String newPublicKey;
    @JsonProperty(value = "new_private_key")
    @BsonIgnore
    private String newPrivateKey;
    @JsonIgnore
    @BsonProperty(value = "user_id")
    private ObjectId userId;
    @JsonProperty(value = "private_key")
    @BsonProperty(value = "private_key")
    private String privateKey;
    @JsonProperty(value = "public_key")
    @BsonProperty(value = "public_key")
    private String publicKey;
}
