package server_monitoring_back.services.agent_service;

import lombok.NoArgsConstructor;
import server_monitoring_back.App;
import server_monitoring_back.services.token_service.TokenService;
import server_monitoring_back.services.token_service.TokenServiceImpl;

@NoArgsConstructor
public class AgentServiceImpl implements AgentService {
    private TokenService tokenService = new TokenServiceImpl();

    public AgentServiceImpl(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    public boolean validateAgent(String body,String sign,String key) {
        String result = tokenService.dataHashing(body, key);
        System.out.println(result);
        return sign.equals(result);
    }
}
