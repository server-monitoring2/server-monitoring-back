package server_monitoring_back.services.token_service;

public interface TokenService {
    String generateToken();
    String dataHashing(String data,String key);
}
