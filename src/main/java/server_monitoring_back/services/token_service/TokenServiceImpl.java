package server_monitoring_back.services.token_service;

import lombok.NoArgsConstructor;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.Random;

public class TokenServiceImpl implements TokenService {
    private final Random random;

    public TokenServiceImpl() {
        this.random = new Random();
    }

    /**
     * Method to generate random string
     *
     * @return String containing random string
     */
    @Override
    public String generateToken() {
        int leftLimit = 48;
        int rightLimit = 122;
        int targetStringLength = 10;
        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
    }

    /**
     * Metod to hash data
     *
     * @param data String containing data you want to hash
     * @param key  String containing secret key used for hashing
     * @return Either hashed data if hashing was successful or null if not
     */
    @Override
    public String dataHashing(String data, String key) {
        byte[] hmacSha256 = null;
        StringBuilder result = new StringBuilder();
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(data.getBytes());
            for (final byte element : hmacSha256) {
                result.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
            }
            System.out.println("Result:[" + result + "]");
            return result.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
