package server_monitoring_back.services.db_service;

import com.mongodb.client.MongoCollection;

public interface DBService{
    void connect();
    MongoCollection getCollection(String collectionName,Object t);
}
