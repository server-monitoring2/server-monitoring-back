package server_monitoring_back.services.db_service;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.AllArgsConstructor;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@AllArgsConstructor
public class DBServiceImpl implements DBService{

    private MongoDatabase database;
    private MongoClient mongoClients;

    public DBServiceImpl() {
        connect();
    }

    public void connect() {
            ConnectionString connString = new ConnectionString("mongodb://localhost:27017/test_database");
            CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
            CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    pojoCodecRegistry);
            MongoClientSettings clientSettings = MongoClientSettings.builder()
                    .applyConnectionString(connString)
                    .codecRegistry(codecRegistry)
                    .build();
            try {
                mongoClients = MongoClients.create(clientSettings);
                database = mongoClients.getDatabase("test_database");
            }catch (Exception e){
                System.out.println(e.toString());
            }

    }

    @Override
    public MongoCollection getCollection(String name, Object t) {
        MongoCollection collection = database.getCollection(name,t.getClass());
        return collection;
    }


}
