package server_monitoring_back.services.task_shedule_service;


import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import server_monitoring_back.models.*;
import server_monitoring_back.repositories.*;
import server_monitoring_back.services.date_service.DateService;
import server_monitoring_back.services.date_service.DateServiceImpl;
import server_monitoring_back.services.site_check_service.SiteCheckService;
import server_monitoring_back.services.site_check_service.SiteCheckServiceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@NoArgsConstructor
public class TaskSheduleServiceImpl implements TaskSheduleService {

    private static final Timer deleteTimer = new Timer();
    private static final Timer urlTimer = new Timer();
    private static final ServerRepository serverRepository = new ServerRepositoryImpl();
    private static final UserRepository userRepository = new UserRepositoryImpl();
    private static final AgentLogRepository agentLogRepository = new AgentLogRepositoryImp();
    private static final SiteLogRepository siteLogRepository = new SiteLogRepositoryImpl();
    private static final SiteRepository siteRepository = new SiteRepositoryImpl();
    private static final DateService dateService = new DateServiceImpl();
    private static final SiteCheckService siteCheck = new SiteCheckServiceImpl();
    private static boolean active, premium;
    private static String pattern = "yyyy/MM/dd HH:mm:ss";
    private static String pattern2 = "yyyy-MM-dd HH:mm:ss Z";
    private static Date dateOfRegistration;
    private static Date dateOfLastFlush;
    private static List<Server> serverList = new ArrayList<>();
    private static List<Site> siteList = new ArrayList<>();
    private UrlChecker urlChecker = new UrlChecker();
    private RemindTask remindTask = new RemindTask();
    private static Date currentDate = new Date();

    public static void checkUser() {
        List<User> userList = new ArrayList<>();
        userRepository.findAll(userList);
        System.out.println(userList);
        try {

        userList.forEach(user -> {
                System.out.println(user);
                userActions(user);
        });
    } catch (Exception e) {
            System.out.println("Error usercheck: " + e.toString());
    }
    }

    public static void checkUrl() {
        List<User> userList = new ArrayList<>();
        userRepository.findAll(userList);
        try {
            for (User user : userList) {
                if(user!=null)urlActions(user);
            }
        } catch (Exception e) {
            System.out.println("Error : checkurl" + e.toString());
        }
    }

    private static void urlActions(User user) {
        List<Site> siteList = siteRepository.getSitesByUserId(user.getId());
        for (Site site : siteList) {
            SiteLog siteLog = new SiteLog();
            siteLog.setUrl(site.getUrl());
            siteLog.setUserId(user.getId());
            siteLog.setAt(dateService.getCurrentDateString());
            siteLog.setCode(0);
            siteLog.setAvailable(siteCheck.checkIfAvailable(site.getUrl(),siteLog));
            siteLogRepository.save(siteLog);
        }
    }

    private static void userActions(User user) {
        premium = user.isPremium();
        active = user.isActive();
        try {
            dateOfRegistration =   new SimpleDateFormat(pattern).parse(user.getDateOfRegistration());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        serverList = serverRepository.getServersByUserId(user.getId());
        siteList = siteRepository.getSitesByUserId(user.getId());
        System.out.println("I`m working" + " user id : " + user.getId());
        System.out.println(serverList);
        for(Server server:serverList){
            Optional<AgentLog> agentLogList = agentLogRepository.findFirstLog(server.getPublicKey());
            System.out.println(agentLogList);
            agentLogList.ifPresent(agentLog -> {
                try {
                    dateOfLastFlush = new SimpleDateFormat(pattern2).parse(agentLog.getAt());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println(agentLog+ " date " + dateService.dateDifference(currentDate, dateOfLastFlush));
                deleteAgentInfo(dateService.dateDifference(currentDate, dateOfLastFlush) / 86400000,server);
            });
        }
            for(Site site:siteList){
                Optional<SiteLog> siteLogs = siteLogRepository
                        .findFirstLogsByUrlAndUserId(site.getUrl(),site.getUserId());
                siteLogs.ifPresent(siteLog -> {
                    try {
                        dateOfLastFlush = new SimpleDateFormat(pattern).parse(siteLog.getAt());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    System.out.println(siteLog+ " date " + dateOfLastFlush);
                    deleteUrlInfo(dateService.dateDifference(currentDate, dateOfLastFlush) / 86400000,site);
                });
            }
        deleteOldUsers(dateService.dateDifference(currentDate, dateOfRegistration) / 60000, user.getId());
    }

    private static void deleteOldUsers(Double difference, ObjectId objectId) {
        if (difference > 10 && !active) {
            userRepository.deleteById(objectId);
        }
    }
    private static void deleteAgentInfo(Double difference, Server servers) {
        if (difference > 3 && !premium) {
            System.out.println("server deleted");
            agentLogRepository.deleteByPublicKey(servers.getPublicKey());
        }
    }
    private static void deleteUrlInfo(Double difference, Site site) {
        if (difference > 3 && !premium) {
            System.out.println("server deleted");
            siteLogRepository.deleteByUrlAndUserId(site.getUrl(),site.getUserId());
        }
    }


    @Override
    public void start() {
        deleteTimer.schedule(remindTask,5000, 10 * 60000);
        urlTimer.schedule(urlChecker,1000, 5 * 60000);
    }

    /**
     * Class for running thread with sheduled operations
     *
     * @author Kaznacheiev Volodymyr
     */
    class RemindTask extends TimerTask {
        public RemindTask() {

        }

        /**
         * Metod to start task shedule
         */
        @Override
        public void run() {
            checkUser();
        }
    }

    class UrlChecker extends TimerTask {
        public UrlChecker() {

        }

        /**
         * Metod to start task shedule
         */
        @Override
        public void run() {
            checkUrl();
        }
    }
}
