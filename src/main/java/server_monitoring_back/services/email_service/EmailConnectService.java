package server_monitoring_back.services.email_service;

import javax.mail.Session;

public interface EmailConnectService {
    void connect();
    Session getSession();
}
