package server_monitoring_back.services.email_service;

import javax.mail.Session;

public interface EmailService {

    void sendMail(String changingText, String to, String text, String subject);
}
