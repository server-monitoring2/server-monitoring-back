package server_monitoring_back.services.email_service;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import server_monitoring_back.App;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@NoArgsConstructor
@AllArgsConstructor
public class EmailServiceImpl implements EmailService {

    private EmailConnectService emailConnectService = App.emailConnectService;



    /**
     * Method for establishing connection to email services
     *
     * @param changingText String array containing changing parameters(such as reset token etc)
     * @param to           String array containing user email
     * @param text         String array containing email text
     * @param subject      String array containing email subject
     */
    @Override
    public void sendMail(String changingText, String to, String text, String subject) {
        try {
            Message message = new MimeMessage(emailConnectService.getSession());
            String from = "TSS2ServerMonitoringService@gmail.com";
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(text + changingText);
            System.out.println("sending...");
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (Exception e) {
            System.out.println("Exception while sending email: " + e.toString());
        }
    }
}
