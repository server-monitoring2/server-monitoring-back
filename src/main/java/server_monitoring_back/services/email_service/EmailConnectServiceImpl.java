package server_monitoring_back.services.email_service;

import lombok.Getter;
import lombok.Setter;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

@Getter
@Setter
public class EmailConnectServiceImpl implements EmailConnectService {
    private Session session;

    public EmailConnectServiceImpl() {
        this.connect();
    }



    /**
     * Method for establishing connection to email services
     */
    @Override
    public void connect() {
        Properties properties = System.getProperties();
        String host = "smtp.gmail.com";
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");
        session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(System.getenv("PROJ_EMAIL")
                        ,System.getenv("PROJ_EMAIL_PASSWORD"));
            }
        });
        session.setDebug(true);
    }
}
