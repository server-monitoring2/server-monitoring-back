package server_monitoring_back.services.validation_service;

import server_monitoring_back.models.SiteLog;

import java.util.List;

public class ValidationServiceImpl implements ValidationService {
    @Override
    public String validateSiteLogs(List<SiteLog> siteLogs) {
        if(siteLogs.size()==0){
            return "{\"error\": \"no logs for now\"}";
        }
        double checkRate =0;
        for(SiteLog siteLog:siteLogs){
            if(siteLog.getAvailable()) checkRate +=1;
        }
        return "{\"check_rate\": \"" + checkRate/siteLogs.size() + "\"," +
                "\"last_info\": \"" + siteLogs.get(siteLogs.size() - 1).getAvailable()+ "\"," +
                "\"last_code\": \""+ siteLogs.get(siteLogs.size() - 1).getCode()+"\",\"" +
                "last_check\": \""+ siteLogs.get(siteLogs.size() - 1).getAt()+"\",\""+
                "check_size\": \"" + (siteLogs.size() - 1) + "\"}";
    }
}
