package server_monitoring_back.services.validation_service;

import server_monitoring_back.models.SiteLog;

import java.util.List;

public interface ValidationService {
    String validateSiteLogs(List<SiteLog> siteLogs);
}
