package server_monitoring_back.services.pic_service;

import io.javalin.http.UploadedFile;
import server_monitoring_back.models.User;

public interface PicService {
    boolean validatePic(UploadedFile uploadedFile,String token,String FileName);
    boolean deleteLastPic(String picName);
}
