package server_monitoring_back.services.pic_service;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import io.javalin.http.UploadedFile;
import lombok.NoArgsConstructor;
import server_monitoring_back.models.User;
import server_monitoring_back.services.date_service.DateService;
import server_monitoring_back.services.date_service.DateServiceImpl;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

@NoArgsConstructor
public class PicServiceImpl implements PicService {

    @Override
    public boolean validatePic(UploadedFile uploadedFile,String token,String FileName) {
        if(uploadedFile == null) {
            return false;
        }
        try (InputStream inputStream = uploadedFile.getContent()) {
            File localFile = new File("src/main/resources/media/" + FileName);
            OutputStream outStream = new FileOutputStream(localFile);
            outStream.write(inputStream.readAllBytes());
            outStream.close();
            String mimetype = new MimetypesFileTypeMap().getContentType(localFile);
            String type = mimetype.split("/")[0];
            if (type.equals("image")) {
                return true;
            } else {
                deleteLastPic(localFile.getName());
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
    }

    @Override
    public boolean deleteLastPic(String picName) {
        if (picName != null) {
            File picPath = new File("src/main/resources/media/" + picName);
                return picPath.delete();
        } else {
            return true;
        }
    }
}
