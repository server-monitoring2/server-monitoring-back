package server_monitoring_back.services.date_service;

import java.util.Date;

public interface DateService {
    String getCurrentDateString();
    String getCurrentDate();
    Double dateDifference(Date firstDate, Date secondDate);
}
