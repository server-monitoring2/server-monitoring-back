package server_monitoring_back.services.date_service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


@NoArgsConstructor
public class DateServiceImpl implements DateService{
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private Calendar calendar;


    @Override
    public String getCurrentDateString() {
        calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }

    @Override
    public String getCurrentDate() {
        calendar = Calendar.getInstance();
        return String.valueOf(calendar.getTimeInMillis());
    }

    @Override
    public Double dateDifference(Date firstDate, Date secondDate) {
        return (double)(firstDate.getTime() - secondDate.getTime());
    }


}
