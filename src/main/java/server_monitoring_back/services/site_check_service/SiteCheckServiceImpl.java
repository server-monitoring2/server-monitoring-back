package server_monitoring_back.services.site_check_service;

import lombok.NoArgsConstructor;
import server_monitoring_back.models.Site;
import server_monitoring_back.models.SiteLog;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

@NoArgsConstructor
public class SiteCheckServiceImpl implements SiteCheckService {
    @Override
    public boolean checkIfAvailable(String url, SiteLog siteLog) {
        boolean available;
        try {
            URL url1 = new URL(url);
            HttpURLConnection connections = (HttpURLConnection)url1.openConnection();
            int code = connections.getResponseCode();
            siteLog.setCode(code);
            siteLog.setAvailable(siteLog.getCode() == 200);
            return true;
        } catch (final MalformedURLException e) {
            return false;
        } catch (final IOException e) {
            System.out.println(e);
            return false;
        }
    }
}
