package server_monitoring_back.services.site_check_service;

import server_monitoring_back.models.Site;
import server_monitoring_back.models.SiteLog;

public interface SiteCheckService {
    boolean checkIfAvailable(String url, SiteLog siteLog);
}
