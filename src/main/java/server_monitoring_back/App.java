package server_monitoring_back;

import io.javalin.http.staticfiles.Location;
import server_monitoring_back.controllers.*;
import io.javalin.Javalin;
import io.javalin.core.util.Header;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.db_service.DBServiceImpl;
import server_monitoring_back.services.email_service.EmailConnectService;
import server_monitoring_back.services.email_service.EmailConnectServiceImpl;
import server_monitoring_back.services.task_shedule_service.TaskSheduleService;
import server_monitoring_back.services.task_shedule_service.TaskSheduleServiceImpl;


import static io.javalin.apibuilder.ApiBuilder.*;
import static com.stripe.Stripe.*;

public class App {

    public static DBService dbService = new DBServiceImpl();
    public static EmailConnectService emailConnectService = new EmailConnectServiceImpl();

    private static TaskSheduleService taskSheduleService = new TaskSheduleServiceImpl();

    public static void main(String[] args) {

        apiKey = "sk_test_51Hc6FDF1TdcqCa4Tcq9xnCQso2K4zxxQw78C9wV7f5SQHPVPplezT9b6swyJxT2soqcz3Bk1trAuf7YypJc0Q6te008mF5VvZE";

        Javalin app = Javalin.create();
        app.config.enableCorsForOrigin("origin");
        app.start(8080);
        app.config.addStaticFiles("/documentation", "target/site/apidocs", Location.EXTERNAL);
        app.config.addStaticFiles("/media", "src/main/resources/media", Location.EXTERNAL);
        app.config.addStaticFiles("/notFound", "src/main/resources/static/Custom", Location.EXTERNAL);

        app.error(404, "html", ctx -> {
            ctx.redirect("/notFound");
        });

        taskSheduleService.start();

        app.before(ctx -> {
            ctx.header(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
            ctx.header(Header.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
            ctx.header(Header.ACCESS_CONTROL_ALLOW_HEADERS, "*");
            ctx.header(Header.ACCESS_CONTROL_ALLOW_METHODS, "*");
        });

        app.routes(() -> {
            before(AbstractController.localizationSetting);
            path("/stripe-webhook", () -> {
                post(StripeController.stripeWebHook);
            });

            path("/login", () -> {
                post(LoginController.userLogin);
            });

            before("/register", RegisterController::checkIfRegistered);
            path("/register", () -> {
                post(RegisterController.userRegister);
            });
            after("/register", RegisterController::sendActivationEmail);

            path("/agent", () -> {
                path("/log", () -> {
                    post(AgentController.addAgentLogs);
                });
            });
            path("/user", () -> {
                before("/payment", StripeController::checkToken);
                path("/payment", () -> {
                    post(StripeController.paymentHandler);
                });
                before("/agent/list/:serverValue", UserController::checkToken);
                path("/agent/list/:serverValue", () -> {
                    post(UserController.getAgentData);
                });
                before("/site/list/*", SiteController::checkToken);
                path("/site/list/*", () -> {
                    get(SiteController.getSiteLog);
                });
                path("/pic", () -> {
                    before("/*",PicController::checkToken);
                    path("/get", () -> {
                        get(PicController.getPic);
                    });
                    path("/upload", () -> {
                        post(PicController.uploadPic);
                    });
                });
                before("/info",UserController::checkToken);
                path("/info", () -> {
                    get(UserController.getUserStatus);
                });
                path("/activate/:activationToken", () -> {
                    get(UserController.activateUser);
                });
                before("/delete", UserController::checkToken);
                path("/delete", () -> {
                    delete(UserController.deleteUser);
                });

                before("/password/change", UserController::checkTokenAndPassword);
                path("/password/change", () -> {
                    post(UserController.changeUserPassword);
                });

                path("/server", () -> {
                    before("/*", ServerController::checkToken);
                    path("/add", () -> {
                        post(ctx -> {
                            ServerController.serverUpdate(ctx, ServerController::addServer);
                        });
                    });
                    path("/change", () -> {
                        put(ctx -> {
                            ServerController.serverUpdate(ctx, ServerController::changeServer);
                        });
                    });
                    path("/delete/:publicKey", () -> {
                        delete(ServerController.deleteServer);
                    });
                });

                path("/url", () -> {
                    before("/*", SiteController::checkToken);
                    path("/add", () -> {
                        post(SiteController.addSite);
                    });
                    path("/delete/*", () -> {
                        delete(SiteController.deleteServer);
                    });
                });
            });
            before("/password/reset", PasswordController::checkUser);
            path("/password", () -> {
                path("/reset", () -> {
                    post(PasswordController.sendResetPasswordEmail);
                    path("/:resetPasswordToken", () -> {
                        put(PasswordController.resetPassword);
                    });
                });
            });
            before("server/names", ServerController::checkToken);
            path("server/names", () -> {
                get(ServerController.getServers);
            });
            before("sites/names", SiteController::checkToken);
            path("sites/names", () -> {
                get(SiteController.getSites);
            });
        });
    }
}
