package server_monitoring_back.repositories;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import org.bson.types.ObjectId;
import server_monitoring_back.App;
import server_monitoring_back.models.Tokens;
import server_monitoring_back.models.User;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.token_service.TokenService;
import server_monitoring_back.services.token_service.TokenServiceImpl;

import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.*;

public class UserRepositoryImpl extends AbstractCrudRepositoryImpl<User> implements UserRepository {

    private static final String PASSWORD_HASH_KEY = System.getenv("PASSWORD_HASH_KEY");

    private final static TokenService tokenService = new TokenServiceImpl();
    private final TokenRepository tokenRepository;
    private final ServerRepository serverRepository;
    private final SiteRepository siteRepository;

    public UserRepositoryImpl(DBService dbService, String collectionName
            , User user) {
        super(dbService, collectionName, user);
        this.tokenRepository = new TokensRepositoryImpl();
        this.serverRepository = new ServerRepositoryImpl();
        this.siteRepository = new SiteRepositoryImpl();
    }

    public UserRepositoryImpl() {
        super(App.dbService, "user_info", new User());
        this.tokenRepository = new TokensRepositoryImpl();
        this.serverRepository = new ServerRepositoryImpl();
        this.siteRepository = new SiteRepositoryImpl();
    }

    @Override
    public Optional<User> findByUsernameAndPassword(String username, String password) {
        return Optional
                .ofNullable((User) mongoCollection
                        .find(and(eq("username", username)
                                , eq("password", tokenService.dataHashing(password, PASSWORD_HASH_KEY))))
                        .first());
    }

    @Override
    public Optional<User> findByUsernameAndEmail(String username, String email) {
        return Optional
                .ofNullable((User) mongoCollection
                        .find(and(eq("username", username)
                                , eq("email", email)))
                        .first());
    }

    @Override
    public Optional<User> findByPassword(String password) {
        return Optional
                .ofNullable((User) mongoCollection
                        .find((eq("password"
                                , tokenService.dataHashing(password, PASSWORD_HASH_KEY))))
                        .first());
    }

    @Override
    public boolean existByUsername(String username) {
        return Optional
                .ofNullable((User) mongoCollection
                        .find(eq("username", username))
                        .first())
                .isPresent();
    }

    @Override
    public boolean existByUsernameOrEmail(String username, String email) {
        return Optional
                .ofNullable((User) mongoCollection
                        .find(or(eq("username", username),eq("email", email)))
                        .first())
                .isPresent();
    }

    @Override
    public User save(User var1) {
        try {
            mongoCollection.insertOne(var1);
            tokenRepository.save(new Tokens(tokenService, var1.getId()));
        } catch (Exception e) {
            System.out.println("Error while writing an object: " + e.toString());
        }
        return var1;
    }

    @Override
    public void deleteById(ObjectId id) {
        super.deleteById(id);
        tokenRepository.deleteByUserId(id);
        serverRepository.deleteByUserId(id);
        siteRepository.deleteByUserId(id);
    }

    @Override
    public void delete(User var1) {
        super.delete(var1);
        tokenRepository.deleteByUserId(var1.getId());
        serverRepository.deleteByUserId(var1.getId());
        siteRepository.deleteByUserId(var1.getId());
    }


    @Override
    public void updatePassword(String newPassword, User user) {
        BasicDBObject set = new BasicDBObject("$set",
                new BasicDBObject("password", newPassword));
        mongoCollection.updateOne(eq("_id", user.getId()), set);
    }

    @Override
    public void setActiveTo(ObjectId userId, Boolean state) {
        BasicDBObject set = new BasicDBObject("$set",
                new BasicDBObject("active", state));
        mongoCollection.updateOne(eq("_id", userId), set);
    }

    @Override
    public void addUserPic(User user,String ref) {
        BasicDBObject set = new BasicDBObject("$set",
                new BasicDBObject("profile_pic", ref));
        mongoCollection.updateOne(Filters.eq("_id", user.getId()), set);
    }

    @Override
    public void activateUser(User user) {
        BasicDBObject set = new BasicDBObject("$set",
                new BasicDBObject("premium", true));
        mongoCollection.updateOne(Filters.eq("_id", user.getId()), set);
    }

    @Override
    public void findAll(List<User> list) {
        mongoCollection.find().forEach(poj->list.add((User)poj));
    }
}
