package server_monitoring_back.repositories;

import server_monitoring_back.models.AgentLog;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface AgentLogRepository extends CrudRepository<AgentLog, ObjectId>{
    List<AgentLog> findLogsByPublicKeyAndSortByDates(String publicKey,String firstDat,String secondDate);
    List<AgentLog> findLogsByPublicKey(String publicKey);
    void deleteByPublicKey(String publicKey);
    Optional<AgentLog> findFirstLog(String publicKey);
}
