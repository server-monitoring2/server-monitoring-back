package server_monitoring_back.repositories;

import io.javalin.http.UploadedFile;
import server_monitoring_back.models.User;
import org.bson.types.ObjectId;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, ObjectId>{
    Optional<User> findByUsernameAndPassword(String username, String password);
    Optional<User> findByUsernameAndEmail(String username, String email);
    Optional<User> findByPassword(String password);
    boolean existByUsername(String username);
    boolean existByUsernameOrEmail(String username,String email);
    void updatePassword(String newPassword,User user);
    void setActiveTo(ObjectId userId,Boolean state);
    void addUserPic(User user,String ref);
    void activateUser(User user);
}
