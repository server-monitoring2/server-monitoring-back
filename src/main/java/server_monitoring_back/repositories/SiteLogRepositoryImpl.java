package server_monitoring_back.repositories;

import org.bson.types.ObjectId;
import server_monitoring_back.App;
import server_monitoring_back.models.AgentLog;
import server_monitoring_back.models.Site;
import server_monitoring_back.models.SiteLog;
import server_monitoring_back.services.db_service.DBService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

public class SiteLogRepositoryImpl extends AbstractCrudRepositoryImpl<SiteLog> implements SiteLogRepository {

    public SiteLogRepositoryImpl(DBService dbService, String collectionName, SiteLog siteLog) {
        super(dbService, collectionName, siteLog);
    }

    public SiteLogRepositoryImpl() {
        super(App.dbService, "site_log", new SiteLog());
    }

    @Override
    public List<SiteLog> findLogsByUrlAndUserId(String url, ObjectId userId) {
        List<SiteLog> siteLogs = new ArrayList<>();
        mongoCollection.find(and(eq("user_id",userId),eq("url",url)))
                .forEach(site->siteLogs.add((SiteLog)site));
        return siteLogs;
    }

    @Override
    public Optional<SiteLog> findFirstLogsByUrlAndUserId(String url, ObjectId userId) {
        return Optional.ofNullable((SiteLog) mongoCollection
                .find(and(eq("user_id",userId),eq("url",url))).first());
    }
    @Override
    public void deleteByUrlAndUserId(String url, ObjectId userId) {
        mongoCollection.deleteMany(and(eq("user_id",userId),eq("url",url)));
    }

}
