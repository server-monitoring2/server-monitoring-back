package server_monitoring_back.repositories;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.model.Filters;
import lombok.AllArgsConstructor;
import org.bson.conversions.Bson;
import server_monitoring_back.App;
import server_monitoring_back.models.AgentLog;
import server_monitoring_back.services.db_service.DBService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.*;

public class AgentLogRepositoryImp extends AbstractCrudRepositoryImpl<AgentLog> implements AgentLogRepository{
    public AgentLogRepositoryImp(DBService dbService, String collectionName, AgentLog agentLog) {
        super(dbService, collectionName, agentLog);
    }

    public AgentLogRepositoryImp() {
        super(App.dbService, "agent_log", new AgentLog());
    }

    @Override
    public List<AgentLog> findLogsByPublicKeyAndSortByDates(String publicKey,String firstDat,String secondDate) {
        Bson query = Filters.and(gte("at",firstDat),lt("at",secondDate),eq("public_key",publicKey));
        List<AgentLog> agentLogList = new ArrayList<>();
        mongoCollection.find(query)
                .forEach(agent->agentLogList.add((AgentLog) agent));
        return agentLogList;
    }

    @Override
    public List<AgentLog> findLogsByPublicKey(String publicKey) {
        List<AgentLog> agentLogList = new ArrayList<>();
        mongoCollection.find(eq("public_key",publicKey))
                .forEach(agent->agentLogList.add((AgentLog) agent));
        return agentLogList;
    }

    @Override
    public void deleteByPublicKey(String publicKey) {
        mongoCollection.deleteMany(eq("public_key",publicKey));
    }

    @Override
    public Optional<AgentLog> findFirstLog(String publicKey) {
        return Optional.ofNullable((AgentLog) mongoCollection.find(eq("public_key",publicKey)).first());
    }
}
