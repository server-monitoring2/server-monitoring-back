package server_monitoring_back.repositories;

import org.bson.types.ObjectId;
import server_monitoring_back.models.Server;

import java.util.List;
import java.util.Optional;

public interface ServerRepository extends CrudRepository<Server, ObjectId> {
    boolean existByPublicKey(String publicKey);
    List<Server> getServersByUserId(ObjectId userId);
    Optional<Server> getServerByPublicKey(String publicKey);
    void updateServer(Server server);
    void deleteByUserId(ObjectId userId);
}
