package server_monitoring_back.repositories;

import org.bson.types.ObjectId;
import server_monitoring_back.models.User;
import server_monitoring_back.models.UserPremiumPayment;

import java.util.Optional;

public interface UserPaymentRepository extends CrudRepository<UserPremiumPayment, ObjectId> {
    Optional<UserPremiumPayment> findUserPaymentByClientSecret(String clientSecret);
    void changeStatus(User user);
}
