package server_monitoring_back.repositories;

import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;
import server_monitoring_back.App;
import server_monitoring_back.models.Tokens;
import server_monitoring_back.services.db_service.DBService;
import server_monitoring_back.services.token_service.TokenService;
import server_monitoring_back.services.token_service.TokenServiceImpl;

import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

public class TokensRepositoryImpl extends AbstractCrudRepositoryImpl<Tokens> implements TokenRepository{

    private final TokenService tokenService = new TokenServiceImpl();

    public TokensRepositoryImpl(DBService dbService, String collectionName, Tokens tokens) {
        super(dbService, collectionName, tokens);
    }

    public TokensRepositoryImpl() {
        super(App.dbService, "user_tokens", new Tokens());
    }

    @Override
    public void deleteByUserId(ObjectId userId) {
        mongoCollection.findOneAndDelete(eq("user_id",userId));
    }

    @Override
    public Optional<Tokens> findByUserId(ObjectId userId) {
        return Optional.ofNullable((Tokens)mongoCollection.find(eq("user_id",userId)).first());
    }

    @Override
    public void setNewResetToken(Tokens tokens) {
        String token = tokenService.generateToken();
        BasicDBObject set = new BasicDBObject("$set",
                new BasicDBObject("password_reset_token",token));
        mongoCollection.updateOne(eq("user_id",tokens.getUserId()),set);
    }

    @Override
    public void setNewActivationToken(Tokens tokens) {
        String token = tokenService.generateToken();
        BasicDBObject set = new BasicDBObject("$set",
                new BasicDBObject("activation_token",token));
        mongoCollection.updateOne(eq("user_id",tokens.getUserId()),set);
    }

    @Override
    public Optional<Tokens> findByResetToken(String resetToken) {
        return Optional.ofNullable((Tokens) mongoCollection
                .find(eq("password_reset_token",resetToken)).first());
    }

    @Override
    public Optional<Tokens> findByActivationToken(String activationToken) {
        return Optional.ofNullable((Tokens) mongoCollection
                .find(eq("activation_token",activationToken)).first());
    }

    @Override
    public Optional<Tokens> findByUserToken(String userToken) {
        return Optional.ofNullable((Tokens) mongoCollection
                .find(eq("user_token",userToken)).first());
    }
}
