package server_monitoring_back.repositories;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import org.bson.types.ObjectId;
import server_monitoring_back.App;
import server_monitoring_back.models.Site;
import server_monitoring_back.services.db_service.DBService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;



public class SiteRepositoryImpl extends AbstractCrudRepositoryImpl<Site> implements SiteRepository{

    private final SiteLogRepository siteLogRepository;

    public SiteRepositoryImpl(DBService dbService, String collectionName, Site site) {
        super(dbService, collectionName, site);
        siteLogRepository = new SiteLogRepositoryImpl();
    }



    public SiteRepositoryImpl() {
        super(App.dbService, "site_info", new Site());
        siteLogRepository = new SiteLogRepositoryImpl();
    }

    @Override
    public List<Site> getSitesByUserId(ObjectId userId) {
        List<Site> siteList = new ArrayList<>();
        mongoCollection.find(eq("user_id",userId))
                .forEach(site->siteList.add((Site) site));
        return siteList;
    }

    @Override
    public void delete(Site var1) {
        super.delete(var1);
        siteLogRepository.deleteByUrlAndUserId(var1.getUrl(), var1.getUserId());
    }

    @Override
    public boolean existByUrl(String url) {
        return Optional
                .ofNullable((Site) mongoCollection
                        .find(eq("url",url))
                        .first()).isPresent();
    }

    @Override
    public boolean existByUrlAndUserId(String url, ObjectId id) {
        return Optional
                .ofNullable((Site) mongoCollection
                        .find(and(eq("url",url),eq("user_id",id)))
                        .first()).isPresent();
    }

    @Override
    public Optional<Site> getSiteByUrl(String url) {
        return Optional
                .ofNullable((Site) mongoCollection
                        .find(eq("url",url)).first());
    }

    @Override
    public void updateSite(Site site) {
        BasicDBObject set = new BasicDBObject("$set", new BasicDBObject("url", site.getNewUrl()));
        mongoCollection.updateMany(
                and(Filters.eq("user_id", site.getUserId()), Filters.eq("url", site.getOldUrl())), set);
    }

    @Override
    public void deleteByUserId(ObjectId userId) {
        mongoCollection.deleteMany(eq("user_id",userId));
    }
}
