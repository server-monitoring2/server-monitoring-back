package server_monitoring_back.repositories;

import org.bson.types.ObjectId;
import server_monitoring_back.models.AgentLog;
import server_monitoring_back.models.SiteLog;

import java.util.List;
import java.util.Optional;

public interface SiteLogRepository extends CrudRepository<SiteLog, ObjectId>{
    List<SiteLog> findLogsByUrlAndUserId(String url,ObjectId userId);
    Optional<SiteLog> findFirstLogsByUrlAndUserId(String url, ObjectId userId);
    void deleteByUrlAndUserId(String url,ObjectId userId);
}
