package server_monitoring_back.repositories;

import org.bson.types.ObjectId;
import server_monitoring_back.models.Tokens;

import java.util.Optional;

public interface TokenRepository extends CrudRepository<Tokens, ObjectId>{
    void deleteByUserId(ObjectId userId);
    Optional<Tokens> findByUserId(ObjectId userId);
    Optional<Tokens> findByResetToken(String resetToken);
    Optional<Tokens> findByActivationToken(String activationToken);
    Optional<Tokens> findByUserToken(String userToken);
    void setNewResetToken(Tokens token);
    void setNewActivationToken(Tokens token);
}
