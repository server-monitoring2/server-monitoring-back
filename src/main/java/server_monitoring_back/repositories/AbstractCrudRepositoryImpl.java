package server_monitoring_back.repositories;

import com.mongodb.client.MongoCollection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import server_monitoring_back.models.BaseEntity;
import org.bson.Document;
import org.bson.types.ObjectId;
import server_monitoring_back.services.db_service.DBService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

public abstract class AbstractCrudRepositoryImpl<T extends BaseEntity> implements CrudRepository<T, ObjectId>{

    protected final DBService dbService;
    protected   MongoCollection mongoCollection;


    public AbstractCrudRepositoryImpl(DBService dbService, String collectionName,T t) {
        this.dbService = dbService;
        this.mongoCollection = dbService.getCollection(collectionName,t);
    }


    public T save(T var1) {
        try{
            mongoCollection.insertOne(var1);
        }catch (Exception e){System.out.println("Error while writing an object: "+e.toString());}
        return var1;
    }

//    public List<T> saveAll(List<T> var1) {
//        try{
//            mongoCollection.insertMany(var1);
//        }catch (Exception e){System.out.println("Error while writing an object: "+e.toString());}
//        return var1;
//    }

    public Optional<T> findById(ObjectId id) {
        return Optional.ofNullable((T)mongoCollection.find(eq("_id",id)).first());
    }

//    public boolean existsById(ObjectId id) {
//        Optional<T> optionalT = findById(id);
//        return optionalT.isPresent();
//    }

    public void findAll(List<T> list) {
        mongoCollection.find().forEach(poj->list.add((T)poj));
    }

//    public List<T> findAllById(List<ObjectId> var1) {
//        List<T> list = new ArrayList<>();
//        for(ObjectId id:var1){
//            if(findById(id).isPresent()) list.add(findById(id).get());
//        }
//        return list;
//    }

//    public long count() {
//        return mongoCollection.countDocuments();
//    }

    public void deleteById(ObjectId id) {
        mongoCollection.findOneAndDelete(eq("_id",id));
    }

    public void delete(T var1) {
        deleteById(var1.getId());
    }

//    public void deleteAll(List<T> var1) {
//        var1.forEach(var->deleteById(var.getId()));
//    }


    public T update(T oldVar, T newVar) {
        mongoCollection.findOneAndReplace(eq("_id",oldVar.getId()),newVar);
        return newVar;
    }

}
