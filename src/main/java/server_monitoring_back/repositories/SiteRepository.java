package server_monitoring_back.repositories;

import org.bson.types.ObjectId;
import server_monitoring_back.models.Server;
import server_monitoring_back.models.Site;

import java.util.List;
import java.util.Optional;

public interface SiteRepository extends CrudRepository<Site, ObjectId>{
    boolean existByUrl(String url);
    boolean existByUrlAndUserId(String url,ObjectId id);
    List<Site> getSitesByUserId(ObjectId userId);
    Optional<Site> getSiteByUrl(String url);
    void updateSite(Site site);
    void deleteByUserId(ObjectId userId);
}
