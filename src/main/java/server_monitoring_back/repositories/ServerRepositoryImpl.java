package server_monitoring_back.repositories;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import org.bson.types.ObjectId;
import server_monitoring_back.App;
import server_monitoring_back.models.Server;
import server_monitoring_back.models.User;
import server_monitoring_back.services.db_service.DBService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

public class ServerRepositoryImpl extends AbstractCrudRepositoryImpl<Server> implements ServerRepository {

    private final AgentLogRepository agentLogRepository = new AgentLogRepositoryImp();

    public ServerRepositoryImpl(DBService dbService, String collectionName, Server server) {
        super(dbService, collectionName, server);
    }

    public ServerRepositoryImpl() {
        super(App.dbService, "server_info", new Server());
    }

    @Override
    public boolean existByPublicKey(String publicKey) {
        return Optional
                .ofNullable((Server) mongoCollection
                        .find(eq("public_key", publicKey))
                        .first())
                .isPresent();
    }

    @Override
    public List<Server> getServersByUserId(ObjectId userId) {
        List<Server> serverList = new ArrayList<>();
        mongoCollection.find(eq("user_id",userId)).forEach(server->serverList.add((Server)server));
        return serverList;
    }

    @Override
    public void updateServer(Server server) {
        BasicDBObject set = new BasicDBObject("$set", new BasicDBObject("public_key", server.getNewPublicKey()));
        BasicDBObject set2 = new BasicDBObject("$set", new BasicDBObject("private_key", server.getNewPrivateKey()));
        mongoCollection.updateMany(
                Filters.and(Filters.eq("user_id", server.getUserId()), Filters.eq("public_key", server.getOldPublicKey())), set2);
        mongoCollection.updateMany(
                Filters.and(Filters.eq("user_id", server.getUserId()), Filters.eq("public_key", server.getOldPublicKey())), set);
    }

    @Override
    public Optional<Server> getServerByPublicKey(String publicKey) {
        return Optional
                .ofNullable((Server) mongoCollection
                        .find(eq("public_key",publicKey)).first());
    }

    @Override
    public void deleteByUserId(ObjectId userId) {
        List<Server> serverList = getServersByUserId(userId);
            for(Server server1 :serverList){
                agentLogRepository.deleteByPublicKey(server1.getPublicKey());
            }
        mongoCollection.deleteMany(eq("user_id",userId));
    }
}
