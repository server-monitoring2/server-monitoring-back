package server_monitoring_back.repositories;

import com.mongodb.BasicDBObject;
import server_monitoring_back.App;
import server_monitoring_back.models.User;
import server_monitoring_back.models.UserPremiumPayment;

import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

public class UserPaymentRepositoryImpl extends AbstractCrudRepositoryImpl<UserPremiumPayment>
        implements UserPaymentRepository {

    public UserPaymentRepositoryImpl() {
        super(App.dbService, "user_premium_payment", new UserPremiumPayment());
    }

    @Override
    public Optional<UserPremiumPayment> findUserPaymentByClientSecret(String clientSecret) {
        return Optional
                .ofNullable((UserPremiumPayment) mongoCollection
                        .find((eq("client_secret",clientSecret)))
                        .first());
    }

    @Override
    public void changeStatus(User user) {
        Optional<UserPremiumPayment> userPremiumPayment = Optional
                .ofNullable((UserPremiumPayment) mongoCollection
                        .find((eq("user_id",user.getId())))
                        .first());
        userPremiumPayment.ifPresent(userOptional->{
            BasicDBObject set = new BasicDBObject("$set",
                    new BasicDBObject("status", !userOptional.isStatus()));
            mongoCollection.updateOne(eq("_id", userOptional.getId()), set);
        });

    }
}
