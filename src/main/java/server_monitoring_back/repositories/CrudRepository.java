package server_monitoring_back.repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T, ID> {
    T save(T var1);

//    List<T> saveAll(List<T> var1);

    Optional<T> findById(ID var1);

//    boolean existsById(ID var1);

     void findAll(List<T> list);
//
//    List<T> findAllById(List<ID> var1);
//
//    long count();

    void deleteById(ID var1);

    void delete(T var1);

//    void deleteAll(List<T> var1);

    T update(T var1, T var2);
}
