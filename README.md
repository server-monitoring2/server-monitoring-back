﻿# Server Monitoring System

Server monitoring system allows you to track current state of your servers, using active and passive checks.
System can send weekly reports 

## Installation
From user's perspective
No installation is needed except for having either Google Chrome or Firefox browsers

From developers perspective:
0. Run script in the folder where you wish to install the project git clone https://tss2020.repositoryhosting.com/git/tss2020/t2.git .
1. Install Java 8 and Maven Javadoc.
2. Install supervisor and mongodb.
3. Install nginx and set up domains.
4. Pull the project and compile to jar - (cd /project_path/) -> git pull -> mvn clean compile assembly:single javadoc:javadoc.

5. Change dir to supervisor and run 2 scripts below.
[program:mongo]
command=mongod
autostart=true
autorestart=true
(launches DB in autonomous mode)

[program:monitoring_back]
directory=/var/www/server-monitoring-back/
command=  java -jar target/server-monitoring-1.0-SNAPSHOT-jar-with-dependencies.jar
autostart=true
autorestart=true
stdout_logfile=/var/www/server-monitoring-back/supervisor_out.log
stderr_logfile=/var/www/server-monitoring-back/supervisor_err.log

## Usage
Navigate to the [website] (https://t2.tss2020.site), register/log in and connect your server using private and public keys


## Contributing
- Volodymyr Kaznacheiev - Back-end
- Andriy Gritsina - Front-end
- Stanislav Shulha - Analyst
- Serhii Lohutov - QA Engineer
- Oleksandr Kibenko - QA Engineer
- Volodymyr Mischenko - Lead

## Built with
[React](https://reactjs.org/) - JavaScript library for building user interfaces or UI components
[Node.js](https://nodejs.org/en/) - JavaScript runtime environment
Java 8 - version of java used during development
[Javalin](https://javalin.io/) - simple web framework
[Maven](https://maven.apache.org/) - фреймворк для автоматизації збірки проекту
[Git](https://git-scm.com) - version control system
[npm](https://www.npmjs.com) - packet managers
[Postman](https://www.postman.com) - API testing tool
[VS Code](https://code.visualstudio.com) - code editor
[Nginx](https://www.nginx.com) - web server
[MongoDB](https://www.mongodb.com) - database
ssh/http/https - protolocs for data transfer
[potapuff/agent](https://github.com/potapuff/agent) - agent provided to us

## Documents 
[SOW](https://docs.google.com/document/d/1CF3Y6A0OpV_nDartqZKGqExg75xHAdra7IPK9210BZg/edit) 
[Project's Board](https://trello.com/b/8oS21lJN/qatssteam2)

## Code standards
[**Oracle Java Code Convention**](https://www.oracle.com/java/technologies/javase/codeconventions-contents.html)

## License
TSSedu

Copyright 2020 ©, team 2

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished 
to do so, subject to the following conditions:
Software can’t be used by other teams at TSS courses.
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.